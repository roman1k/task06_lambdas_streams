package lambdas;

@FunctionalInterface
public interface FuncInterface {
    int method(int a, int b, int c);
}
