package lambdas;

public class Main {

    public static void main(String[] args) {
        FuncInterface summ  =  (a,  b, c) -> a+b+c;
        FuncInterface arg =  (a,  b, c) ->  (a+b+c)/3;
        int a = summ.method(5,9,123);
        System.out.println(a);
        int b = arg.method(9,4,6);
        System.out.println(b);
}

    }

